#!/usr/bin/env bash

# Standard dependencies
sudo pacman -S firefox pulseaudio pavucontrol vim neovim kitty zsh git locate

sudo updatedb # this is for locate

# General dependencies
paru -Sy todo-bin papirus-icon-theme acpi acpid \
acpi_call wireless_tools jq inotify-tools polkit-gnome xdotool xclip \
brightnessctl alsa-utils alsa-tools pulseaudio pulseaudio-alsa scrot \
redshift mpd mpc mpdris2 ncmpcpp playerctl ffmpeg bluez-utils gpick --needed

# mpd 
systemctl --user enable mpd.service
systemctl --user start mpd.service

# battery
sudo systemctl enable acpid.service
sudo systemctl start acpid.service

# Install icommon font
wget -O /tmp/icommon.zip https://www.dropbox.com/s/hrkub2yo9iapljz/icomoon.zip?dl=0 &&
	sudo unzip -d /usr/share/fonts /tmp/icommon.zip && \
	sudo mv /usr/share/fonts/icommon/*.ttf /usr/share/fonts && \
	sudo rm -rf /usr/share/fonts/icomoon

# Install other fonts
paru -S nerd-fonts-jetbrains-mono ttf-font-awesome ttf-font-awesome-4 ttf-material-design-icons ttc-iosevka

# Install zsh plugins
paru -S zsh-syntax-highlighting zsh-autosuggestions

# Install interesting tools
paru -S lsd bat

# Install software that I use to create applications and other stuff
# Install java
# https://linuxconfig.org/how-to-install-java-on-manjaro-linux
sudo pacman -S jre11-openjdk-headless jre11-openjdk jdk11-openjdk openjdk11-doc openjdk11-src
# Install maven
wget -O /tmp/apache-maven-3.6.3.tar.gz https://downloads.apache.org/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz && \
	sudo tar -C /opt -xvzf /tmp/apache-maven-3.6.3.tar.gz && \
	echo "export MAVEN_HOME=/opt/apache-maven-3.6.3" >> ~/.zshenv && \
	echo "export PATH=\$PATH:\$MAVEN_HOME/bin" >> ~/.zshenv && \
	source ~/.zshenv

# Install snapd and dependencies using it
sudo git clone https://aur.archlinux.org/snapd.git /opt/snapd && \
	sudo chown -R mrtimeout:mrtimeout /opt/snapd && \
	cd /opt/snapd && makepkg -si && \
	sudo systemctl enable --now snapd.socket && \
	sudo ln -s /var/lib/snapd/snap /snap && \
	sudo snap install intellij-idea-community --classic && \
	sudo snap install teams

# Clone some repos
test -d ~/github/inditex || { echo "Creating \$HOME/github/inditex"; mkdir --parent ~/github/inditex; }

while IFS= read -r repo; do
	dir_repo=$(cut -d '/' -f2 <<< "$repo" | cut -d '.' -f1)
	echo "Cloning $repo into ~/github/inditex/$dir_repo"
	git clone $repo ~/github/inditex/$dir_repo
	cd ~/github/inditex/$dir_repo/code && mvn clean compile
done < secret_repo.txt
