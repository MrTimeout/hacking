# Installing Arch linux

## Partitions

### Creating

We have to create partitions using `cfdisk`:

- This partitions will be dos if we are using a virtualization.
- Create primary partition of _512 MB_.
- Create primary partition where will reside all your data (the biggest part)
- Create primary partition which will be the swap partition. We have
to change the type of this partition to Linux swap/solaris.
- Use write option to persist modifyications.

### Formatting

After this, we create the fs for each of this partitions:

- `mkfs.vfat -F 32 /dev/sda1` -> format to fat 32 for boot partition.
- `mkfs.ext4 /dev/sda2` -> format to ext4 partition number 2.
- `mkswap /dev/sda3; swapon` -> create the swap partition.

### Mounting

Then, we have to mount this partitions with the following fs.

- `mount /dev/sda2 /mnt`
- `mkdir /mnt/boot/efi`
- `mount /dev/sda1 /mnt/boot/efi`
- `pacstrap /mnt linux linux-firmware networkmanager grub wpa_supplicant base base-devel vim nano neofetch efibootmgr`

### Persisting

`genfstab -U /mnt > /mnt/etc/fstab`

## Entering in the mounted fs

We enter inside it: `arch-chroot /mnt`

Creating passwd for root user: `passwd`

Creating another user: `useradd -m mrtimeout`

Assigning a password to it: `passwd mrtimeout`

Install sudo package using: `pacman -S sudo`

Assigning group _wheel_ to user mrtimeout: `usermod -aG wheel mrtimeout`

Modifying file `/etc/sudoers` so we can execute command as our user when using sudo.
We have to modify the line 82 and uncomment it: `sed -i '82s/^#*//' /etc/sudoers`.

Establish the locale to your favourite uncomenting the line of the file `/etc/locale.gen`

If you want to see if there are any by default, use this command: `cat --number /etc/locale.gen | sed 's/#.*/d'`

We can set the keyboard layout by default: `echo "KEYMAP=en" > /etc/vconsole.conf`

## Grub

First step we need to accomplish is to install the grub: `grub-install /dev/sda`

Create the configuration using: `grub-mkconfig -o /boot/grub/grub.cfg`

## Editing more files before the reboot

```sh
# We modify the hostname to our personal.
echo arch4u > /etc/hostname
# We add some dns for our hosts, so we can resolve localhost to the usuals
cat <<EOF >> /etc/hosts

127.0.0.1   localhost
::1         localhost
127.0.0.1   arch4u.localhost arch4u
# We execute the command that all we use jeje
neofetch
```

Here we exit from the `arch-chroot` and `reboot` the machine.

```sh
sudo su

systemctl start NetworkManager.service
systemctl enable NetworkManager.service
systemctl start wpa_supplicant.service
systemctl enable wpa_supplicant.service

pacman -S git kitty xorg xorg-server

git clone https://aur.archlinux.org/paru-bin.git /opt/paru-bin
chown -R mrtimeout:mrtimeout /opt/paru-bin
cd /opt/paru-bin && makepkg -si

mkdir /opt/blackarch && chwon -R mrtimeout /opt/blackarch
curl -O /opt/blackarch/strap.sh https://blackarch.org/strap.sh
chmod +x /opt/blackarch/strap.sh && ./strap.sh
# It will print all the categories that we have inside blackarch
pacman -Sg | grep blackarch | awk '{print $1;}' | sort -u | less

systemctl enable gdm.service
systemctl start gdm.service
```
