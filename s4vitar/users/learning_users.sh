#!/usr/bin/env bash

## We can output all the shells allowed in a Linux OS using this command.
cat /etc/shells | grep '^/' | awk -F '/' '{print $NF}'
cat /etc/shells | grep '^/' | rev | cut -d '/' -f 1 | rev

## /etc/passwd is where info about users is located.
cat /etc/passwd | grep 'sh$' | awk -F ':' '{print $1, $NF}' # Outputs the user with the shell that is assigned to it.

## We can use the id command to get: uid, gid and groups.
id | sed 's/ /\n/g'
## We can use groups to get all the groups where the user is located.
groups | sed 's/ /\n/g'
